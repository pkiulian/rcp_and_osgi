package rcp.example.internal.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



import rcp.example.api.IDegreeAmplifier;
import rcp.example.api.IDegreeAmplifierListener;

public class DegreeAmplifier implements IDegreeAmplifier{

	// some default value;
	private int degree = 0;
	
	private List<IDegreeAmplifierListener> listeners = new ArrayList<>();
	
	@Override
	public void degreeUp() {
		setDegree(getDegrees() + 1);
		notifyDegreeChanged();
		
	}

	@Override
	public void degreeDown() {
		setDegree(getDegrees() - 1);
		notifyDegreeChanged();		
	}
	
	private void notifyDegreeChanged() {
		synchronized (listeners) {
			Iterator<IDegreeAmplifierListener> iterator = listeners.iterator();
			while (iterator.hasNext()) {
				IDegreeAmplifierListener listener = iterator.next();
				listener.degreeChanged();
			}
		}
	}

	@Override
	public void addListener(IDegreeAmplifierListener listener) {
		synchronized (listeners) {
			if (!listeners.contains(listener)) {
				listeners.add(listener);
			}
		}
		
	}

	@Override
	public void removeListener(IDegreeAmplifierListener listener) {
		synchronized (listeners) {
			if (listeners.contains(listener)) {
				listeners.remove(listener);
			}
		}
	}
	
	@Override
	public int getDegrees() {
		return degree;
	}

	private void setDegree(int degree) {
		this.degree = degree;
	}

	public int getListenersSize() {
		return this.listeners.size();
	}
}
