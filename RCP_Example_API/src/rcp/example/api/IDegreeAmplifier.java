package rcp.example.api;

public interface IDegreeAmplifier {
	
	public void addListener(IDegreeAmplifierListener listener);

	public void removeListener(IDegreeAmplifierListener listener);
	
	public void degreeUp();
	
	public void degreeDown();

	public int getDegrees();	
	
}
