package rcp_example;

import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

public class View1 extends ViewPart {
	public static final String ID = "RCP_Example.view";

	private Composite mainComposite;
	private Text text;

	@Inject
	DegreeChanger dc;

	@Override
	public void createPartControl(Composite parent) {
		dc.startUp();
		// create a FormLayout and set its margin
		mainComposite = new Composite(parent, SWT.NONE);
		FormLayout layout = new FormLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 10;

		// set layout for parent
		mainComposite.setLayout(layout);

		manageButtons();
		manageText();
	}

	@Override
	public void dispose() {
		super.dispose();
		dc.shutDown();
	}

	private void manageText() {
		text = new Text(mainComposite, SWT.READ_ONLY | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		RGB rgb = new RGB(225, 225, 204);
		Color color = new Color(rgb);
		text.setBackground(color);
		// Set up the position of the button on the mainComposite
		FormData positionAndSizeButton = new FormData();
		positionAndSizeButton.top = new FormAttachment(15, 0);
		positionAndSizeButton.bottom = new FormAttachment(100, 0);
		positionAndSizeButton.left = new FormAttachment(0, 0);
		positionAndSizeButton.right = new FormAttachment(100, 0);

		// set positionAndSizeButton for button
		text.setLayoutData(positionAndSizeButton);
	}

	private void manageButtons() {
		Button increaseDegButton = new Button(mainComposite, SWT.PUSH);
		increaseDegButton.setText("Press To increase Temperature");
		increaseDegButton.setBackground(new Color(232, 109, 109));

		Button decreaseDegButton = new Button(mainComposite, SWT.PUSH);
		decreaseDegButton.setText("Press To Decrease Temperature");
		decreaseDegButton.setBackground(new Color(118, 232, 109));

		decreaseDegButton.addListener(SWT.Selection, event -> {
			text.append("Button Pressed -> Decrease Temperature! \n");
			dc.getDegreeAmplifierService().degreeDown();
		});

		increaseDegButton.addListener(SWT.Selection, event -> {
			text.append("Button Pressed -> Increaase Temperature! \n ");
			dc.getDegreeAmplifierService().degreeUp();
		});

		// Set up the position of the button on the mainComposite
		FormData incDeg = new FormData();
		incDeg.top = new FormAttachment(0, 0);
		incDeg.bottom = new FormAttachment(10, 0);
		incDeg.left = new FormAttachment(0, 0);
		incDeg.right = new FormAttachment(50, 0);

		FormData decDeg = new FormData();
		decDeg.top = new FormAttachment(0, 0);
		decDeg.bottom = new FormAttachment(10, 0);
		decDeg.left = new FormAttachment(52, 0);
		decDeg.right = new FormAttachment(100, 0);

		// set positionAndSizeButton for button
		increaseDegButton.setLayoutData(incDeg);
		decreaseDegButton.setLayoutData(decDeg);
	}

	@Override
	public void setFocus() {
		mainComposite.setFocus();
	}

}