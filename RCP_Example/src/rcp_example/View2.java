package rcp_example;

import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

public class View2 extends ViewPart {
	public static final String ID = "RCP_Example.view2";
	
	private Composite mainComposite;
	
	private Text textView2;
	
	@Inject DegreeChanger dc;

	@Override
	public void createPartControl(Composite parent) {
		dc.startUp();
		mainComposite = new Composite(parent, SWT.NONE);	
		FormLayout layout = new FormLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		// set layout for parent
		mainComposite.setLayout(layout);
		
		manageText();
		
		dc.setTextLog2(this.textView2);
	}

	
	@Override
	public void dispose() {
		super.dispose();
		dc.shutDown();
	}
	
	@Override
	public void setFocus() {
		mainComposite.setFocus();		
	}
	
	private void manageText() {
		textView2 = new Text(mainComposite, SWT.READ_ONLY | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL );
		RGB rgb= new RGB(225, 225, 204);
		Color color = new Color(rgb);
		textView2.setBackground(color );
		// Set up the position of the button on the mainComposite
				FormData positionAndSizeButton = new FormData();
				positionAndSizeButton.top = new FormAttachment(0, 0);
				positionAndSizeButton.bottom = new FormAttachment(100, 0);
				positionAndSizeButton.left = new FormAttachment(0, 0);
				positionAndSizeButton.right = new FormAttachment(100, 0);

				// set positionAndSizeButton for button
				textView2.setLayoutData(positionAndSizeButton);
	}

}
