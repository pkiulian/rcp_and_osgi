package rcp_example;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.swt.widgets.Text;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import rcp.example.api.IDegreeAmplifier;
import rcp.example.api.IDegreeAmplifierListener;

@Creatable
@Singleton
public class DegreeChanger implements IDegreeAmplifierListener {

	int degree;

	private Text textView2;

	private IDegreeAmplifier degreeAmplifierService;

	public DegreeChanger() {
		BundleContext context = ModelHolder.getInstance().getBundleContext();
		ServiceReference<?> serviceRef = context.getServiceReference(IDegreeAmplifier.class.getName());
		degreeAmplifierService = (IDegreeAmplifier) context.getService(serviceRef);
	}

	public void startUp() {
		this.degreeAmplifierService.addListener(this);
	}

	public void shutDown() {
		this.degreeAmplifierService.removeListener(this);

	}

	@Override
	public void degreeChanged() {
		updateDegree();
	}

	private void updateDegree() {
		this.degree = this.degreeAmplifierService.getDegrees();

		if (this.textView2 != null) {
			this.textView2.append("Current Degree: " + this.degree + " \n ");
		}
	}
	

	public IDegreeAmplifier getDegreeAmplifierService() {
		return this.degreeAmplifierService;
	}

	public void setTextLog2(Text textView2) {
		this.textView2 = textView2;
	}

}
