package rcp.example.internal.impl.test;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import rcp.example.api.IDegreeAmplifierListener;
import rcp.example.internal.impl.DegreeAmplifier;


import static org.easymock.EasyMock.*;

class DegreeAmplifierTest {

	static DegreeAmplifier da;
	static IDegreeAmplifierListener amplifListener;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		da = new DegreeAmplifier();
		amplifListener = createMock(IDegreeAmplifierListener.class);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test 
	void addListenerTest() {
		int oldNumber = da.getListenersSize();
		da.addListener(amplifListener);
		int newNumber = da.getListenersSize();
		assertEquals(oldNumber+1, newNumber);
	}
	
	@Test 
	void removeListenerTest() {
		int oldNumber = da.getListenersSize();
		da.removeListener(amplifListener);
		int newNumber = da.getListenersSize();
		assertEquals(oldNumber-1, newNumber);
	}
	
	@Test
	void testDegreeUp() {				
		int oldDegree = da.getDegrees();
		da.degreeUp();
		int newDegree = da.getDegrees();
		assertEquals(oldDegree+1, newDegree);
	}
	
	@Test
	void testDegreeDown() {
		int oldDegree = da.getDegrees();
		da.degreeDown();
		int newDegree = da.getDegrees();
		assertEquals(oldDegree-1, newDegree);
	}

}
